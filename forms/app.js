/**
 * Exercice 1 :
 * - Vérifiez que le dom est bien créé
 * - Sélectionnez l'input par son id
 * - Ajoutez un événement lorsque la valeur de l'input change
 * - Stockez la valeur du contenu
 * - Créez et sélectionnez une balise <p></p> via son id
 * - Insérez ce contenu dans la balise p créé
 */

/**
 * Exercice 2 :
 * - Vérifiez que le dom est bien créé
 * - Sélectionnez tous les items de la liste par leur class
 * - Stocker un item de la list
 * - Supprimez l'item de la liste
 * - Créez un nouvel élément li
 * - créé un text avec la valeur de l'item (qui a été supprimé mais stocké)
 * - Sélectionnez et stocker le conteneur ul via son id
 * - Ajouter un nouveau li dans ce container
 */

/**
 * Exercice 3 :
 * Au téléchargement complet de la page
 * l'input doit être automatiquement focus
 */
