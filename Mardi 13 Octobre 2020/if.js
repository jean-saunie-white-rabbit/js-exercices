// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/if...else

/*
    --------------------------
    If..else - Les conditions
    --------------------------
*/

/**
 * Exercice 1 :
 * Écrire le script qui
 * Afficher dans la console "ok" si la variable cond1 est true
 */
let cond1 = true;

/**
 * Exercice 2 :
 * Écrire le script qui
 * Afficher dans la console "ok" si la variable var2 est supérieur à 10
 */
let var2 = 10;

/**
 * Exercice 3 :
 * Écrire le script qui
 * Afficher dans la console "ok" si la variable var3 est inférieur ou égale à 200
 */
let var3 = 10;

/**
 * Exercice 4 :
 * Écrire le script qui
 * Afficher dans la console "ok" si la variable cond4 est false
 */
let cond4 = false;

/**
 * Exercice 5 :
 * Écrire le script qui
 * Si la variable cond5 est true affiche "ok" dans la console ou sinon affiche "non" dans la cas contraire
 */
let cond5 = true;

/**
 * Exercice 6 :
 * Écrire le script qui
 * Si la variable cond6 est égale à "partir" affiche "au revoir" dans la console
 * ou sinon affiche "bonjour" dans la cas contraire
 */
let cond6 = 'partir';

/**
 * Exercice 7 :
 * Affichez dans la console l'inverse de cond7 et
 * créer une condition pour afficher "Il est vrai"
 * dans la console si l'inverse de cond7 est true
 */
let cond7 = 1;
