// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String

/*
    --------------------------
    Les chaînes de charactères
    --------------------------
*/
let str = 'Ma string';

/**
 * Exercice 1 :
 * Affichez dans la console la lettre à la position 2
 */

/**
 * Exercice 2 :
 * Affichez dans la console la taille de cette string
 */

/**
 * Exercice 3 :
 * Créez un tableau contenant les mots de cette phrase en séparant les mots par les espaces
 * Exemple :
 *  string -> "Hi John Doe"
 *  expected -> ["hi", "John", "Doe"]
 */

/**
 * Exercice 4 :
 * Concaténez deux chaines de caractères entre elles et afficher le resultat dans la console
 */

/**
 * Exercice 4 Bis :
 * Insérez une variable dans une chaîne de charactères et affichez le résultat dans la console
 * Remarque : Ne pas concaténer
 */


/**
 * Exercice 5 :
 * Supprimez les 5 derniers charactères de la chaine
 *  string -> "Hi John Doe"
 *  expected -> "Hi Joh"
 */

/**
 * Exercice 6 :
 * Remplacer le mot "oranges" par "bananes", en utilisant une fonction bien sûr 😅
 */

/**
 * Exercice 7 :
 * Trouvez la position du mot "orange"
 */

/**
 * Exercice 8 :
 * Vérifier si la première phrase contient le mot "baleine"
 */

/**
 * Exercice 9 :
 * Affichez dans la console cette phrase en minuscule, puis en majuscule
 */

/**
 * Exercice 10 :
 * Effacez les espaces inutiles et affichez le résultat dans la console
 *  string -> "   Hi John Doe   "
 *  expected -> "Hi John Doe"
 */
