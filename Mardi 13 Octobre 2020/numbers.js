// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Number

/*
    --------------------------
    Numbers - Les Nombres
    --------------------------
*/

/**
 * Exercice 1 :
 * Déclarer une variable stockant le numbre de votre choix et affichez le nombre et son type dans la console
 */

/**
 * Exercice 2 :
 * Convertissez cette chaine de charactère en nombre et affichez le résultat dans la console
 */

/**
 * Exercice 3 :
 * Convertissez cet nombre en chaine de charactères et affichez le résultat dans la console
 */
let number3 = 10;

/**
 * Exercice 4 :
 * Déclarer une variable contenant un nombre aléatoire et affichez le résultat dans la console
 */

/**
 * Exercice 5 :
 * Arroundissez ce nombre vers l'entier le plus proche et affichez le résultat dans la console
 */
let number6 = 232.23;
