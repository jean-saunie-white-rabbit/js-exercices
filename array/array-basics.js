/**
 * Exercice 1 :
 * Créez un tableau
 */

/**
 * Exercice 2
 * Créez un tableau avec des valeurs initiales : "orange", "red", "pink", "blue"
 */

/**
 * Exercice 3
 * Créez un tableau ramplie de false et un maximum de 10 entrées
 */

/**
 * Exercice 4
 * Créez un tableau et ajoutez-y plusieurs valeurs
 */

/**
 * Exercice 5
 * Créez un ntableau et affichez la valeur deuxième valeur
 */

/**
 * Exercice 6
 * Créez un tableau et ajoutez-y 10 éléments et supprimez la deuxième valeur
 */

/**
 * Exercice 7 :
 * Créez un tableau et ajoutez-y 10 éléments et supprimez la première valeur
 */
/**
 * Exercice 8 :
 * Créez un tableau et ajoutez-y 10 éléments et supprimez la dernière valeur
 */

/**
 * Exercice 9 :
 * Créez une matrice (un tableau à deux dimension x et y) et initialisez les valeurs
 * de x et y à 0
 */

/**
 * Exercice 10 :
 * Créez une matrice (un tableau à deux dimension x et y) et
 * affichez toute les valeurs
 */

/**
 * Exercice 11 : Bonus
 * Immutabilité
 */

