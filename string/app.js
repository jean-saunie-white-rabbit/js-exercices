// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String

/**
 * Exercice 1 :
 * Affichez la lettre à la position 2
 */

/**
 * Exercice 2 :
 * Affichez la taille de cette string
 */
/**
 * Exercice 3 :
 * Créez un tableau contenant les mots de cette phrase
 * Exemple :
 *  string -> "Hi John Doe"
 *  expected -> ["hi", "John", "Doe"]
 */

/**
 * Exercice 4 :
 * Concaténez les chaines de caractères entre elle
 */

/**
 * Exercice 4 :
 * Insérez une variable dans la variable phrase et affichez phrase
 * Remarque : Ne pas concaténer
 */


/**
 * Exercice 5 :
 * Supprimez les 5 derniers charactères de la chaine
 */

/**
 * Exercice 6 :
 * Remplacer le mot "oranges" par "bananes", en utilisant une fonction bien sûr 😅
 */

/**
 * Exercice 7 :
 * Trouvez la position du mot "orange"
 */

/**
 * Exercice 8 :
 * Vérifier si la première phrase contient le mot "baleine"
 */

/**
 * Exercice 9 :
 * Affichez cette phrase en minuscule, puis en majuscule
 */

/**
 * Exercice 10 : Bonus
 * Effacez les espaces inutiles
 */

/**
 * Exercice 11 : Bonus
 * Écrire cette phrase en :
 * - SnakeCase
 * - Kebab-case
 * - CamelCase
 */

// Ou

