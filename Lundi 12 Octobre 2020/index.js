/*
    Questions :
    1. De quand date la première version de Javascript ?
    2. Quel est l'objectif de javascript, à quel besoin réponds-t-il ?
    3. Quel est l'utilisation du javascript ?
    4. Le javascript est-il un langage bas niveau ou haut niveau ? Et pourquoi ?
    5. Qu'est-ce que le typage dynamique ?
    6. Quels sont les différents type de variables ?
    7. Qu'est-ce qu'un polyfill ?
    8. Qu'est-ce qu'un "transpiler" ? Et Citez moi s'en un ou deux transpiller javascript.
    9. Qu'est-ce qu'une instruction et comment sont-elles délimitées ?
    10. Qu'es-ce que la porté d'une variable ?
 */

/*
    Répondre ICI (dans le commentaire)
    à toutes les questions en numérotant et espaçant vos réponses.
*/

/*
    Exercice 1 :
    Déclare une variable nommé "age" contenant le nombre 24
    Déclare une constante nommé "name" contenant le nom (la chaîne de charactère) de ton choix
*/

// Exercice 2 : Affiche dans la console de ton navigateur les variables "age" et "name"

//  Exercice 3 : Multiplie la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 72

//  Exercice 4 : Soustrais la variable "age" avec "age" et affiche le résultat dans la console
//  Résultat souhaité : 0

//  Exercice 5 : Divise la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 8

/*
    Exercice 6 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" moins 3
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 21

/*
    Exercice 7 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" multiplié par 10
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 210

/*
    Exercice 8 :
    - Déclare une variable "age2" et assigne la valeur 50
    - Puis affiche la variable "age2" dans la console
*/

/*
    Exercice 9 :
    - Additionne "age" avec "age2"
    - Puis affiche le resultat de l'addition dans la console
*/
//  Résultat souhaité : 260

// C'est un exemple pour montrer la marche à suivre sur les prochains exercices.
// Exercice Exemple : Affiche dans la console le résultat de : "age" est-il inférieur à "age2" ?
// Réponsé éxemple : console.log ('Exercice exemple :', age < age2);

// Exercice 9 : Affiche dans la console le résultat de : "age" est-il supérieur à "age2" ?

// Exercice 10 : Affiche dans la console le résultat de : "age2" est-il inférieur à "age" ?

// Exercice 11 : Affiche dans la console le résultat de : "age2" est-il supérieur ou égale à 50 ?

// Exercice 12 : Affiche dans la console le résultat de : "age" est-il inférieur ou égale à 10 ?

// Exercice 13 : Affiche dans la console le résultat de : "age" est-il égale à 210 ?

// Exercice 14 : Affiche dans la console le résultat de : "age" est-il strictement égale en valeur et en type à 210 ?

// Exercice 15 : Déclare une variable de ton choix sans lui affecter de valeur et affiche le résultat dans la console.

// Exercice 16 : Affiche dans la console le type de toutes les variables déclarées précédemment.

// Exercice 17 : Ecrire un script dans la balise "head" du fichier index.html de ce dossier qui affiche la chaine de charactères "head"

// Exercice 18 : Ecrire un script à la fin de la balise "body" du fichier index.html de ce dossier qui affiche la chaine de charactères "body"

// Exercice 19: Créer un fichier app.js dans ce dossier qui affiche dans la console la chaine de charactères "app.js" et importer le dans le fichier index.html de ce dossier

class User {
    constructor(name){
    }
}
const user = new User('Jean');
console.log(user.name);
