// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/function

/**
 * Exercice 1 :
 * créer une fonction avec un argument, qui affiche l’argument
 * appelez la fonction
 */
function myFunction(name) {
    console.log(name);
}

myFunction('Ok');

/**
 * Exercice 2 :
 * créer une fonction qui prend un nombre en argument et
 * qui le multiplie par deux et retourne le résultat.
 * Affichez le résultat dans la console
 */
function numberFn(nb) {
    return nb * 2;
}

console.log(numberFn(3));
console.log(numberFn(4));

/**
 * Exercice 3 :
 * créer une fonction qui détermine si le nombre passé en argument est pair ou impaire.
 */

function pairOrImpaire(nb) {
    // Le modulo % renvoie le reste
    // Si nb est impaire nb % 2 => 1
    // Si nb est paire nb % 2 => 0
    if (!(nb % 2)) { // Impaiire nb % 2 = true
        console.log(nb, 'est pair');
    } else {
        console.log(nb, 'est impaire');
    }
}

pairOrImpaire(5);
pairOrImpaire(10);

/**
 * Exercice 4 :
 * Créer une fonction qui s’invoque elle-même
 */

(function (nb) {
    console.log('selftInv', nb);
})(1);

/**
 * Exercice 5 :
 * Affichez la liste des arguments dans un tableau,
 * sans directement utiliser les arguments en eux même
 */
function ex5(nb1, nb2, nb3, nb4) {
    for (let i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
}

ex5(1, 23, 4, 6, 7, 345, 6, 232, 23, 2);

/**
 * Exercice 6 :
 * Créer deux fonction de votre choix et appelez la première fonction dans la deuxième
 * Puis exécutez la deuxième fonction
 */

function fn1(str) {
    console.log(str);
}

function fn2(str) {
    fn1(str)
}

fn2("Ma string");







