// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array

/*
    --------------------------
    Array - Les tableaux
    --------------------------
*/

/**
 * Exercice 1 :
 * Créez un tableau
 */
let arr1 = [];
console.log('Ex 1 :', arr1);

/**
 * Exercice 2
 * Créez un tableau avec des valeurs initiales : "orange", "red", "pink", "blue"
 */
let arr2 = ["orange", "red", "pink", "blue"];
console.log('Ex 2 :', arr2);

/**
 * Exercice 3
 * Créez un tableau remplie de false et un maximum de 10 entrées
 */
let arr3 = new Array(10).fill(true);
console.log('Ex " :', arr3);

/**
 * Exercice 4
 * Créez un tableau et ajoutez-y plusieurs valeurs
 */
let arr4 = [];
arr4.push('ok');
arr4.push(123);
arr4.push(["oke"]);
console.log('Ex 4 :', arr4);

/**
 * Exercice 5
 * Créez un tableau et affichez la deuxième valeur
 */
let arr5 = [];
arr5.push('ok'); // 0
arr5.push(234); // 1
arr5.push(['ok']); // 2
arr5[2].push("OK");
console.log('Ex 5 :', arr5[2]);

/**
 * Exercice 6
 * Créez un tableau et ajoutez-y 10 éléments et supprimez la deuxième valeur
 */
let arr6 = [];
for (let i = 0; i < 10; i++) {
    // Les instructions dans l'itération
    arr6.push(i);
}
arr6.splice(1, 1);
console.log('Ex 6 :', arr6);

/**
 * Exercice 7 :
 * Créez un tableau et ajoutez-y 10 éléments et supprimez la première valeur
 */
let arr7 = [];
for (let i = 0; i < 10; i++) {
    arr7.push(i);
}
arr7.shift();
console.log('Ex 7 :', arr7);

/**
 * Exercice 8 :
 * Créez un tableau et ajoutez-y 10 éléments et supprimez la dernière valeur
 */
let arr8 = [];
for (let i = 0; i < 10; i++) {
    arr8.push(i);
}
arr8.pop();
console.log('Ex 8 :', arr8);


/**
 * Exercice 9 :
 * Créez un tableau et ajoutez grace à une boucle for 10 données dans le tebleau
 */
let arr9 = [];
for (let i = 0; i < 10; i++) {
    arr9.push("tour : " + i);
}
console.log('Ex 9 :', arr9);

/**
 * Exercice 10 :
 * Utilisez le tableau de l'exercice 9 et grace à une boucle, boucler dans le tableau
 * pour afficher tous les éléments du tableau dans la console
 */
for (let i = 0; i < arr9.length; i++) {
    let item = arr9[i];
    // Manipulation
    console.log('Ex 10 :', arr9[i]);
}

/**
 * Exercice 11 : Bonus
 * Rechercher ce qu'est l'immutabilité et comment y remédier en javascript
 */
let arr11 = ["ok"];
let copie = []; // Spread opérateur

for (let i = 0; i < arr11.length; i++){
    copie.push(arr11[i]);
}

arr11.splice(0, 1);

console.log('arr11 :', arr11, copie);
