// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/for

/**
 * Exercice 1 :
 * En utilisant une boucle for, affichez une suite de nombre allant de 0 à 30
 */

for (let i = 0; i <= 30; i++) {
    // console.log(i);
}

/**
 * Exercice 2 :
 * Créez une boucle qui commence à 0 et incrémente à chaque fin de tour tant que i est inférieur à 100
 * Dans la boucle for créer une condition avec le mot clé if
 * Tant que la variable i est inférieur à 31 affichez Premier mois,
 * ou alors si i est supérieur à 30 et inférieur à 60 affichez Deuxième mois,
 * sinon affichez Reste de l’année 
 */

for (let i = 0; i < 100; i++) {
    // if ( i < 31) {
    //     console.log('Premier mois');
    // } else if (i > 30 && i < 60) {
    //     console.log('Deuxième mois');
    // } else {
    //     console.log('Reste de l\'année');
    // }
}

/**
 * Exercice 3 :
 * Créez une boucle qui commence à 100 et décrément à chaque fin de tour, tant que i est supérieur à 0
 */

for (let i = 100; i >= 0; i--) {
    // console.log(i);
}


/**
 * Exercice 4 :
 * Créez une boucle qui commence à 0 et incrémente à chaque fin de tour tant que i est inférieur à 20
 * Dans chaque tour insérez la valeur de i dans le tableau.
 * Objectif : avoir une tableau de nombre qui correspond à tous les tours effectué par la boucle for
 */
let arr = [];

for (let i = 0; i < 20; i++) {
    arr.push(i);
}

console.log(arr);
