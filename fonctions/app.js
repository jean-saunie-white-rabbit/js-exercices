/**
 * Exercice 1 :
 * créer une fonction avec un argument, qui affiche l’argument
 */

/**
 * Exercice 2 :
 * créer une fonction qui prend un nombre en argument et
 * qui le multiplie par deux et retourne le résultat.
 */

/**
 * Exercice 3 :
 * créer une fonction qui détermine si le nombre passé en argument est pair ou impaire.
 */

/**
 * Exercice 4 :
 * Créer une fonctionne qui s’invoque elle-même
 */

/**
 * Exercice 5 :
 * Affichez la liste des arguments dans un tableau,
 * sans directement utiliser les arguments en eux même
 */

/**
 * Exercice 6 : Bonus
 * Reproduire la suite de fibonacci : 1-1-2-3-5-8-13-21
 */








