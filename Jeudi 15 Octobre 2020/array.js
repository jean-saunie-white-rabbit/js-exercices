// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array

function getRandomArray(limit = 30) {
    let arr = [];
    for (let i = 0; i < limit; ++i) {
        arr.push(Math.floor(Math.random() * 10));
    }
    return arr;
}

/**
 * Exercice 1 :
 * Multipliez par deux chaque valeur du tableau arr
 */
let arr = getRandomArray();

/**
 * Exercice 2 :
 * Créez un tableau avec le retour fonction getRandomArray() et triez les valeurs
 */

/**
 * Exercicec 3 :
 * Créez un table avec le retour fonction getRandomArray() et additionnez toutes les valeurs entre elles
 * En utilisant la méthode .reduce()
 */

/**
 * Exercice 4 :
 * Créez un tableau avec le retour fonction getRandomArray() et filtrez seulement les valeurs pair puis ensuite impaire
 */

/**
 * Exercice 5 :
 * En utilisant la méthode .some() déterminez si le tableau letters contiens la lettre "e"
 * et s'il contien afficheez dans la console "Oui il contiens" sinon affichez non il ne contient pas
 */
let letters = ['j', 'e', 'p', 'm' , 'e', 'e'];


/**
 * Exercice 6 :
 * En utilisant la méthode .map() multipliez chaque valeur par 10
 * puis addiitionnez tous les nombres du tableau créé
 * et affichez le nouveau tableau créé
 */
let nbs6 = [5, 7, 8, 5, 3, 2, 9, 0, 6, 4];

/**
 * Exercice 7 :
 * Créez un table avec le retour fonction getRandomArray()
 * En utilisant la méthode .forEach() Affichez dans la console chaque élément multiplié par 10
 */


/**
 * Exercice 8 :
 * Ecrire une fonction nommé "checkIfExist" qui prend en paramètre "letter" qui est une string
 * Cette fonction doit vérifier sii la string "letter" passée en paramètre éxiste dans le tableau
 * ['j', 'e', 'p', 'm' , 'e', 'e']
 * et elle retourne le résultant de la méthode .some()
 * Utilisez la méthode .some() et .includes()
 */


/**
 * Exercice 9 :
 * En utilisant la méthode .every() Vérifiiez que chaque élément est supérieur à 1
 */
let nbs9 = [5, 7, 8, 5, 3, 2, 9, 2, 6, 4];

/**
 * Exercice 10 :
 * Affichez dans la console le tableau inversé
 */
let nbs10 = [5, 7, 8, 5, 3, 2, 9, 2, 6, 4];
