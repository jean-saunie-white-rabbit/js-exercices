// Manipuler des tableaux

let arr = ['o', 'a', 'b', 'o', 'o'];
let arrNb = [2 ,4 ,1 ,5, 2, 7];
console.log(arr);


// Méthodes applicable à un tableau
// .forEach() Execute une fonction pour chaque élément du tableau
// .map() Modifier un tableau
// .reduce() Execute une fonction pour chaque élément et recréer une valeur "accumulation"
// .filter() Filtrer un tableau - Execute une fonction pour chaque élément et filtre l'élement si la fonction return false
// .some() Dans mon tableau il existe au moins 1 fois l'élément recherché
// .sort() Trier un tableau












// .sort()
// Trier un tableau
let sortedArray = arrNb.sort();
console.log(sortedArray);















// .some()
// Retourne true ou false
// Si le tableau contien au minimum 1 élement recherché
let resSome = arr.some(function(value, index) {
    // console.log(value, index);
    if (value === 'f') {
        return true;
    } else {
        return false;
    }
});











// .reduce()
// Créer une autre valeur à partir d'un tableau
// let arrReduce = [1, 2, 4, 5, 6];
let arrReduce = ['j', 'e', 'a', 'n'];

// Créer une nouvelle valeur
// Un nombre = 18

let res = arrReduce.reduce(function(accumalation, value, index) {
    return accumalation + value; // 12 + 6  = 18
}, "");
// console.log('res :', res);












// .filter()
// Filtrer les éléments d'un tableau
let classe = ["Jean", 'Carmen', 'Adrien', 'Carmen'];
let resultat = classe.filter(function (prenom, index) {
    if (prenom.includes('z')) {
        return true;
    } else {
        return false;
    }
});

console.log(classe, resultat);







// .map()
// Pour modifier un tableau
// Créer nouveau tableau
let mappedArray = arr.map(function (val, index) {
    return val.toUpperCase();
});



// .forEach()
// Traverser le tableau et executer une fonction
// pour chaque élément du tableau
arr.forEach(function (value, index) {
    // console.log(value, index);
});

let posts = [
    {title: 'Titre 1'},
    {title: 'Titre 2'},
    {title: 'Titre 3'},
];
posts.forEach(function (value) {
    // console.log(value.title);
});
