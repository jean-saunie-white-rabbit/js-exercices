// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Object

/**
 * Exercice 1 :
 * Créer un objet dans une variable nommé "student" avec comme propriété âge, name et notes (un tableau de note (nombre))
 */

/**
 * Exercice 2 :
 * Affichez l'âge du student dans la console
 * avec les deux manières montré ce matin pour accéder à une propriété
 */

/**
 * Exercice 3 :
 * Modifiez l'age du student et afficher le résultat dans la console
 */

/**
 * Exercice 4 :
 * Ajouter à l'object "student" une méthode nommer "showInfo" qui affiche dans la console
 * le nom et l'age du student en question
 * et executer la méthode
 */

/**
 * Exercice 5 :
 * Affichez la meilleur note du student
 * Faites une recherche sur internet pour trouver la solution
 */

/**
 * Exercice 6 :
 * Affichez toutes les propriétés dans un tableau
 * Faites une recherche sur internet pour trouver la solution
 */

/**
 * Exercice 7 :
 * Affichez toutes les propriétés dans un tableau
 */

/**
 * Exercice 9 :
 * Créez une fonction nommé "studentFactory" qui prend en paramètre "name", "age"
 * et qui retourne un nouveau object avec comme propriété "name" et "age"
 * Exécutez 2 fois la fonction studentFactory et afficher le retour des deux fonction dans la console
 */

