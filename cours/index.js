// Variables
// Variable : La référence et la valeur
// Un casier qui porte un nom et
// a une valeur à l'intérieur

/* Type :
    - String => Chaine de charactères
    - Object
    - Number
    - Array
 */

// Qui peut-être variable

let present = true; // Number
const absent = false; // String

// Opérateur arithmétique : +-/*%
// Opérateur de comparaison : ==, ===, !=, !==
// Opérateur de comparaison : >, <, >=, <=
// Opérateur de logiques : && , || , !

console.log(present && absent); // ET
console.log(present || absent); // OU
console.log(!present); // inverse


